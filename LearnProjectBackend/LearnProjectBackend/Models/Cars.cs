﻿namespace LearnProjectBackend.Models;

public class Cars
{
    public int Id { get; set; }
    public string Brand { get; set; }
    public string Model { get; set; }
    public bool Repair { get; set; }
    public bool Custom { get; set; }
    public DateTime CreatingDate { get; set; }
    public DateTime EndDate { get; set; }
    public decimal ServicePrice { get; set; }
}