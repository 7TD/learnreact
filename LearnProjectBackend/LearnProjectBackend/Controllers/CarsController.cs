﻿using LearnProjectBackend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LearnProjectBackend.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CarsController : Controller
{
    private readonly AppDbContext _dbContext;

    public CarsController(AppDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    [HttpPost]
    public async Task AddCar([FromBody] Cars cars)
    {
        await _dbContext.Cars.AddAsync(new Cars
        {
            Brand = cars.Brand,
            Model = cars.Model,
            Repair = cars.Repair,
            Custom = cars.Custom,
            CreatingDate = cars.CreatingDate,
            EndDate = cars.EndDate,
            ServicePrice = cars.ServicePrice
        });
        await _dbContext.SaveChangesAsync();
    }

    [HttpGet]
    public async Task<List<Cars>> GetCars()
    {
        return await _dbContext.Cars.ToListAsync();
    }

    [HttpPut]
    public async Task PutCar([FromBody] Cars car)
    {
        await _dbContext.Cars.Where(x => x.Id == car.Id).ExecuteUpdateAsync(p => p.SetProperty(c => c.Brand, c => car.Brand)
                                                                                    .SetProperty(c => c.Model, c => car.Model)
                                                                                    .SetProperty(c => c.Repair, c => car.Repair)
                                                                                    .SetProperty(c => c.Custom, c => car.Custom)
                                                                                    .SetProperty(c => c.CreatingDate, c => car.CreatingDate)
                                                                                    .SetProperty(c => c.EndDate, c => car.EndDate)
                                                                                    .SetProperty(c => c.ServicePrice, c => car.ServicePrice));
    }

}