import axios from 'axios';
import { Car } from '../models/TableInfo';

export class CarsApi {
    apiUrl: string = 'https://localhost:7095/api/Cars';

    getCars2 () {
        return axios.get<Car[]>(this.apiUrl);
    }

    async getCars () {
        return await axios.get<Car[]>(this.apiUrl);
    }

    async putCar (car: Car) {
        let status = 0;
        await axios
            .put(this.apiUrl, car)
            .then((response) => {status = response.status})
            .catch((error) => {status = error.response.status});
        return status;
    }

}
