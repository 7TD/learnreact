import styles from './Login.module.scss';
import {Button, TextField} from "@mui/material";

export const Login = () => {

    return(
        <>
            <div className={styles.Content}>
                <div className={styles.ContentFields}>
                    <TextField
                        className={styles.TextField}
                        id="outlined-basic"
                        label="Login"
                        variant="outlined"
                    />
                    <TextField
                        className={styles.TextField}
                        id="outlined-basic"
                        label="Password"
                        variant="outlined"
                    />
                </div>
                <Button
                    className={styles.Button}
                    variant="contained"
                    size="large"
                >Enter</Button>
            </div>
        </>
    );
}





