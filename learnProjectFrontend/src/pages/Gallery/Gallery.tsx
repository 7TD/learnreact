import styles from './Gallery.module.scss';
import Triangle from '../../assets/svg/Triangle.svg';
import RefreshCircleImage from '../../assets/svg/RefreshCicle.svg';
import React from 'react';
import {ImageApi} from '../../api/imageRequest';
import {ImageList} from "../../components/ImageList/ImageList";
import {ImageInfo} from "../../models/ImageInfo";
import {Button} from "@mui/material";
import RefreshIcon from '@mui/icons-material/Refresh';


export const Gallery = () => {
    const [selectedImage, setSelectedImage] = React.useState<string>();
    const [img, setImg] = React.useState(null);

    const [images, setImages] = React.useState<ImageInfo[]>([]);
    const [activeImageId, setActiveImageId] = React.useState<number>(1);

    // Запрос изображений с сервера
    const getImages = async () => {
        let response = await new ImageApi().getImages();
        setImages(response.data);
    }
    React.useEffect( () => {
        getImages();
    }, [])

    const handleChange = (event: any) => {
        setSelectedImage(URL.createObjectURL(event.target.files[0]));
        setImg(event.target.files[0]);
    }

    // Добавление
    const addImage = async () => {
        let data = new FormData();
        img && data.append('img', img);
        await new ImageApi().addImage(data);
    }

    // Кнопки переключения картинок
    const nextImage = () => {
        const index = images.findIndex(x => x.id === activeImageId);
        if (index === images.length - 1) {
            setActiveImageId(images[0].id);
        }
        else {
            setActiveImageId(images[index + 1].id);
        }
    }
    const previousImage = () => {
        const index = images.findIndex(x => x.id === activeImageId);
        if (index === 0) {
            setActiveImageId(images[images.length - 1].id);
        }
        else {
            setActiveImageId(images[index - 1].id);
        }
    }

    return (
        <>
            <div className={styles.ContentBody}>
                <div className={styles.ContentBodyTitleImage}>
                    {selectedImage ? (
                        <div className={styles.Image}>
                            <img src={selectedImage} alt="image"/>
                        </div>
                    ) : (<div className={styles.Image} />)}
                    <span>TitleImage</span>
                    <div className={styles.TitleImageBtns}>
                        <Button
                            className={styles.Button}
                            variant="contained"
                            component="label"
                        >UPLOAD
                            <input onChange={handleChange} hidden accept="image/" multiple type="file"/>
                        </Button>
                        <Button
                            onClick={() => setSelectedImage('')}
                            className={styles.Button}
                            variant="contained"
                            color="error"
                            size="large"
                        >Cancel</Button>
                    </div>
                    <div className={styles.ContentBodyTitleImagePush}>
                        <Button
                            className={styles.ButtonPush}
                            variant="contained"
                            size="large"
                            onClick={addImage}
                        >Save image</Button>
                    </div>
                </div>
                <div className={styles.ContentBodyList}>
                    <ImageList
                        images={images}
                        setActiveImageId={setActiveImageId}
                        activeImageId={activeImageId}
                    />
                    <div className={styles.ListSelector}>
                        <div className={styles.Image}>
                            <img src={`data:image/jpeg;base64,${images[images.findIndex(x => x.id === activeImageId)]?.photo}`} alt="image" />
                        </div>
                        <div className={styles.Selector}>
                            <img onClick={previousImage} className={styles.TriangleLeft} src={Triangle} alt="Triangle"/>
                            <p>image-name.png</p>
                            <img onClick={nextImage} className={styles.TriangleRight} src={Triangle} alt="Triangle"/>
                        </div>
                        <Button
                            onClick={getImages}
                            className={styles.Button}
                            variant="contained"
                            size="large"
                        >
                            <RefreshIcon className={styles.RefreshIcon}/>
                            Update list
                        </Button>
                    </div>
                </div>
            </div>
        </>
    );
}
























