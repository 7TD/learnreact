import React from "react";
import {Box, Checkbox, TableCell, tableCellClasses, TableHead, TableRow, TableSortLabel} from "@mui/material";
import { visuallyHidden } from '@mui/utils';
import {  HeadCell, Car, Order} from "../../models/TableInfo";
import {styled} from "@mui/material/styles";

interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, newOrderBy: keyof Car) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
}

const headCells: readonly HeadCell[] = [
    {
        id: 'id',
        numeric: false,
        disablePadding: true,
        label: 'Id',
    },
    {
        id: 'brand',
        numeric: true,
        disablePadding: false,
        label: 'Calories',
    },
    {
        id: 'model',
        numeric: true,
        disablePadding: false,
        label: 'Brand',
    },
    {
        id: 'repair',
        numeric: true,
        disablePadding: false,
        label: 'Repair',
    },
    {
        id: 'custom',
        numeric: true,
        disablePadding: false,
        label: 'Custom',
    },
    {
        id: 'creatingDate',
        numeric: true,
        disablePadding: false,
        label: 'Creating Date',
    },
    {
        id: 'endDate',
        numeric: true,
        disablePadding: false,
        label: 'End Date',
    },
    {
        id: 'servicePrice',
        numeric: true,
        disablePadding: false,
        label: 'Service Price',
    },
];

export default function EnhancedTableHead(props: EnhancedTableProps) {
    const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (newOrderBy: keyof Car) => (event: React.MouseEvent<unknown>) => {
        onRequestSort(event, newOrderBy);
    }

    const StyledTableCell = styled(TableCell)(({theme}) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: '#e7e7e7',
            color: theme.palette.common.black,
        },
    }));

    return (
        <TableHead>
            <TableRow>
                <StyledTableCell
                    padding="checkbox"
                >
                    <Checkbox
                        color="primary"
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ 'aria-label': 'select all' }}
                    />
                </StyledTableCell>
                {headCells.map((headCell) => (
                    <StyledTableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                        sortDirection={orderBy === headCell.id ? order: false}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ? (
                                <Box component="span" sx={visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </Box>
                            ) : null}
                        </TableSortLabel>
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}





































