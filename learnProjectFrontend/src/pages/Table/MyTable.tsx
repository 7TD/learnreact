import {
    Box, Checkbox,
    Container,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableFooter,
    TablePagination,
    TableRow,
} from '@mui/material';
import React from 'react';
import TablePaginationActions from './TablePaginationActions';
import EnhancedTableHead from './EnhancedTableHead';
import { Car, Order } from '../../models/TableInfo';
import EnhancedTableToolbar from './EnhancedTableToolbar';
import { CarsApi } from '../../api/carsRequest';

const DEFAULT_ORDER = 'asc';
const DEFAULT_ORDER_BY = 'id';
const DEFAULT_ROWS_PER_PAGE = 5;

function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key,
): (
    a: { [key in Key]: number | string | boolean | Date },
    b: { [key in Key]: number | string | boolean | Date },
) => number {
    return order === 'desc'
        ? (a, b) => descendingComparator(a, b, orderBy)
        : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
        const order = comparator(a[0], b[0]);
        if (order !== 0) {
            return order;
        }
        return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
}


export const MyTable = () => {
    const [rows, setRows] = React.useState<Car[]>([]);

    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(5);

    const [order, setOrder] = React.useState<Order>(DEFAULT_ORDER);
    const [orderBy, setOrderBy] = React.useState<keyof Car>(DEFAULT_ORDER_BY);
    const [selected, setSelected] = React.useState<readonly number[]>([]);
    const [visibleRows, setVisibleRows] = React.useState<Car[] | null>(null);
    const [paddingHeight, setPaddingHeight] = React.useState(0);

    const getCars = async () => {
        const response = await new CarsApi().getCars2();
        setRows(response.data);
    }

    const setVisible = () => {
        let rowsOnMount = stableSort(
            rows,
            getComparator(DEFAULT_ORDER, DEFAULT_ORDER_BY),
        );
        rowsOnMount = rowsOnMount.slice(
            0 * DEFAULT_ROWS_PER_PAGE,
            0 * DEFAULT_ROWS_PER_PAGE + DEFAULT_ROWS_PER_PAGE,
        );
        setVisibleRows(rowsOnMount);
    }

    React.useEffect(() => {
        getCars();
    }, []);

    React.useEffect(() => {
        setVisible();
    }, [rows]);

    const handleChangePage = React.useCallback(
        (event: unknown, newPage: number) => {
            setPage(newPage);

            const sortRows = stableSort(rows, getComparator(order, orderBy));
            const updateRows = sortRows.slice(
                newPage * rowsPerPage,
                newPage * rowsPerPage + rowsPerPage,
            );
            setVisibleRows(updateRows);

            const numEmptyRows = newPage > 0 ? Math.max(0, (1 + newPage) * rowsPerPage - rows.length) : 0;

            const newPaddingHeight = 53 * numEmptyRows;
            setPaddingHeight(newPaddingHeight);
        },
        [order, orderBy, rowsPerPage, rows]
    );

    const handleChangeRowsPerPage = React.useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const updateRowsPerPage = parseInt(event.target.value, 10);
            setRowsPerPage(updateRowsPerPage);

            setPage(0);

            const sortedRows = stableSort(rows, getComparator(order, orderBy));
            const updatedRows = sortedRows.slice(
                0 * updateRowsPerPage,
                0 * updateRowsPerPage + updateRowsPerPage,
            );
            setVisibleRows(updatedRows);

            setPaddingHeight(0);
        },
        [order, orderBy, rows]
    );

    const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            const newSelected = rows.map((c) => c.id);
            setSelected(newSelected);
            return;
        }
        setSelected([]);
    };

    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    const handleClick = (event: React.MouseEvent<unknown>, name: number) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected: readonly number[] = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const handleRequestSort = React.useCallback(
        (event: React.MouseEvent<unknown>, newOrderBy: keyof Car) => {
            const isAsc = orderBy === newOrderBy && order === 'asc';
            const toggledOrder = isAsc ? 'desc' : 'asc';
            setOrder(toggledOrder);
            setOrderBy(newOrderBy);

            const sortedRows = stableSort(rows, getComparator(toggledOrder, newOrderBy));
            const updateRows = sortedRows.slice(
                page * rowsPerPage,
                page * rowsPerPage + rowsPerPage,
            );
            setVisibleRows(updateRows);
        },
        [order, orderBy, page, rowsPerPage, rows],
    );

    return (
        <>
            <div>
                <Container maxWidth="xl" sx={{  }}>
                    <Box sx={{ width: '100%', bgcolor: 'white', display: 'flex', justifyContent: 'center' }}>
                        <Paper sx={{ width: '95%', mb: 2 }}>
                            <EnhancedTableToolbar numSelected={selected.length} />
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 750 }} aria-label="Table">
                                    <EnhancedTableHead
                                        numSelected={selected.length}
                                        onRequestSort={handleRequestSort}
                                        onSelectAllClick={handleSelectAllClick}
                                        order={order}
                                        orderBy={orderBy}
                                        rowCount={rows.length}
                                    />
                                    <TableBody>
                                        {visibleRows
                                            ? visibleRows.map((row, index) => {
                                                const isItemSelected = isSelected(row.id);
                                                const labelId = `enhanced-table-checkbox-${index}`;

                                                return (
                                                    <TableRow
                                                        hover
                                                        onClick={(event) => handleClick(event, row.id)}
                                                        role="checkbox"
                                                        aria-checked={isItemSelected}
                                                        tabIndex={-1}
                                                        key={row.id}
                                                        selected={isItemSelected}
                                                        sx={{ cursor: 'pointer' }}
                                                    >
                                                        <TableCell padding="checkbox">
                                                            <Checkbox
                                                                color="primary"
                                                                checked={isItemSelected}
                                                                inputProps={{ 'aria-labelledby': labelId }}
                                                            />
                                                        </TableCell>
                                                        <TableCell
                                                            component="th"
                                                            id={labelId}
                                                            scope="row"
                                                            padding="none"
                                                        >
                                                            {row.id}
                                                        </TableCell>
                                                        <TableCell align="right">{row.brand}</TableCell>
                                                        <TableCell align="right">{row.model}</TableCell>
                                                        <TableCell align="right">{row.repair ? 'true' : 'false'}</TableCell>
                                                        <TableCell align="right">{row.custom ? 'true' : 'false'}</TableCell>
                                                        <TableCell align="right">{/*row.creatingDate.toUTCString()*/}</TableCell>
                                                        <TableCell align="right">{/*row.endDate.toUTCString()*/}</TableCell>
                                                        <TableCell align="right">{row.servicePrice}</TableCell>
                                                    </TableRow>
                                                );
                                            })
                                            : null}
                                        {paddingHeight > 0 && (
                                            <TableRow
                                                style={{ height: paddingHeight }}
                                            >
                                                <TableCell colSpan={6} />
                                            </TableRow>
                                        )}
                                    </TableBody>
                                    <TableFooter>
                                        <TableRow>
                                            <TablePagination
                                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                                colSpan={12}
                                                count={rows.length}
                                                page={page}
                                                rowsPerPage={rowsPerPage}
                                                SelectProps={{
                                                    inputProps: {
                                                        'aria-label': 'row per page',
                                                    },
                                                    native: true,
                                                }}
                                                onPageChange={handleChangePage}
                                                onRowsPerPageChange={handleChangeRowsPerPage}
                                                ActionsComponent={TablePaginationActions}
                                            />
                                        </TableRow>
                                    </TableFooter>
                                </Table>
                            </TableContainer>
                        </Paper>
                    </Box>
                </Container>
            </div>
        </>
    );
};





































