import styles from './Questionnaire.module.scss';
import Field from '../../components/Field/Field';
import { Button, Container } from "@mui/material";
import React from "react";


export const Questionnaire = () => {
    const [image, setImage] = React.useState<string>();

    const handleChange = (event: any) => {
        setImage(URL.createObjectURL(event.target.files[0]));
    }

    return (
        <>
            <Container maxWidth="xl" sx={{ bgcolor: "white" }}>
                <div className={styles.Content}>
                    <div className={styles.ContentLeft}>
                        <div className={styles.ContentLeftImg}>
                            <img src={image} alt="image" />
                        </div>
                        <Button variant="text" component="label">
                            UPLOAD
                            <input onChange={handleChange} hidden accept="image/" multiple type="file" />
                        </Button>
                    </div>
                    <div>
                        <div className={styles.ContentFields}>
                            <Field lable="FirstName" />
                            <Field lable="LastName" />
                            <Field lable="MiddleName" />
                            <Field lable="Email" />
                            <Field lable="Birthdate" />
                            <Field lable="PhoneNumber" />
                            <Field lable="Scope of work" />
                        </div>
                        <div className={styles.btns}>
                            <Button
                                className={styles.Button}
                                variant="contained"
                            >Save</Button>
                            <Button
                                className={styles.Button}
                                variant="contained"
                                color="error"
                            >Cancel</Button>
                        </div>
                    </div>
                </div>
            </Container>
        </>
    );
}






