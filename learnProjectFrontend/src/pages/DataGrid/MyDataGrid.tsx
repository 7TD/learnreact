import { CarsApi } from '../../api/carsRequest';
import React from 'react';
import { Car } from '../../models/TableInfo';
import Box from '@mui/material/Box';
import { DataGrid, GridColDef, GridRowModel, GridToolbar } from '@mui/x-data-grid';
import { Alert, Snackbar } from '@mui/material';


export interface SnackbarMessage {
    message: string;
    key: number;
}
export interface State {
    open: boolean;
    snackPack: readonly SnackbarMessage[];
    messageInfo?: SnackbarMessage;
}

const columns: GridColDef[] = [
    {field: 'id', headerName: 'ID', width: 90},
    {
        field: 'brand',
        headerName: 'Brand',
        width: 150,
        editable: true,
    },
    {
        field: 'model',
        headerName: 'Model',
        width: 150,
        editable: true,
    },
    {
        field: 'repair',
        headerName: 'Repair',
        description: 'Repair work was carried out',
        width: 100,
        editable: true,
    },
    {
        field: 'custom',
        headerName: 'Custom',
        description: 'Custom work was carried out',
        width: 100,
        editable: true,
    },
    {
        field: 'creatingDate',
        headerName: 'Creating Date',
        width: 200,
        editable: true,
    },
    {
        field: 'endDate',
        headerName: 'End Date',
        width: 200,
        editable: true,
    },
    {
        field: 'servicePrice',
        headerName: 'Service Price',
        type: 'number',
        width: 150,
        editable: true,
    }
];


export const MyDataGrid = () => {
    const [rows, setRows] = React.useState<Car[]>([]);

    const [snackPack, setSnackPack] = React.useState<readonly SnackbarMessage[]>([]);
    const [open, setOpen] = React.useState(false);
    const [messageInfo, setMessageInfo] = React.useState<SnackbarMessage | undefined>(undefined);

    React.useEffect(() => {
        getCars();
    }, []);

    React.useEffect(() => {
        if (snackPack.length && !messageInfo) {
            setMessageInfo({...snackPack[0]});
            setSnackPack((prev) => prev.slice(1));
            setOpen(true);
        } else if (snackPack.length && messageInfo && open) {
            setOpen(false);
        }
    }, [snackPack, messageInfo, open]);

    const handleClick = (message: string) => {
        setSnackPack((prev) => [...prev, { message, key: new Date().getTime() }]);
    };

    const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    const handleExited = () => {
        setMessageInfo(undefined);
    };

    const severitySelecter = (message: string) => {
        if (message === 'The changes were saved successfully') return 'success';
        else if (message === 'No changes have been made') return 'info';
        else if (message === 'An error occurred while saving the data') return 'error';
    }

    async function getCars() {
        const response = await new CarsApi().getCars();
        setRows(response.data);
    }

    const putCar = async (car: Car) => {
        const a = await new CarsApi().putCar(car);
        if (a === 200) {
            handleClick('The changes were saved successfully');
        }
        else {
            handleClick('An error occurred while saving the data');
        }
    }

    const comparison = (obj1: any, obj2: any) => {
        for (const key in obj1) {
            if(obj1[key] !== obj2[key]) {
                return false;
            }
        }
        return true;
    }

    const processRowUpdate = (newRow: GridRowModel) => {
        const updatedRow = { ...newRow, isNew: false };

        let data: Car = { 
            id: newRow.id, 
            brand: newRow.brand, 
            model: newRow.model, 
            repair: newRow.repair, 
            custom: newRow.custom, 
            creatingDate: newRow.creatingDate,
            endDate: newRow.endDate, 
            servicePrice: newRow.servicePrice 
        }
        const newRows = rows.map((row) => (row.id === data.id ? data : row));

        if (comparison(data, rows[rows.findIndex(item => item.id === data.id)])) {
            handleClick('No changes have been made');
        } else {
            setRows(newRows);
            putCar(data);
        }   

        return updatedRow;
    };


    

    return (
        <>
            <Snackbar
                key={messageInfo ? messageInfo.key : undefined}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose} 
                TransitionProps={{ onExit: handleExited }}
            >
                <Alert severity={severitySelecter(messageInfo ? messageInfo.message : '')} sx={{ width: '100%' }}>
                    {messageInfo ? messageInfo.message : undefined}
                </Alert>
            </Snackbar>

            <Box sx={{ height: 406, width: '94%', padding: '3%', bgcolor: 'white', display: 'flex', justifyContent: 'center' }}>
                <DataGrid 
                    rows={rows} 
                    columns={columns}
                    initialState={{
                        pagination: {
                            paginationModel: {
                                pageSize: 5,
                            },
                        },
                    }}
                    slots={{
                        toolbar: GridToolbar,
                    }}
                    pageSizeOptions={[5]}
                    checkboxSelection
                    disableRowSelectionOnClick
                    processRowUpdate={processRowUpdate}              
                />
            </Box>
        </>
    );
}




























