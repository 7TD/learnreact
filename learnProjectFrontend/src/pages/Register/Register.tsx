import styles from './Register.module.scss';
import {Button, TextField} from "@mui/material";


export const Register = () => {


    return(
        <>
            <div className={styles.Content}>
                <div className={styles.ContentFields}>
                    <TextField
                        className={styles.TextField}
                        id="outlined-basic"
                        label="Login"
                        variant="outlined"
                    />
                    <TextField
                        className={styles.TextField}
                        id="outlined-basic"
                        label="Email"
                        variant="outlined"
                    />
                    <TextField
                        className={styles.TextField}
                        id="outlined-basic"
                        label="Password"
                        variant="outlined"
                    />
                    <TextField
                        className={styles.TextField}
                        id="outlined-basic"
                        label="Repeat the password"
                        variant="outlined"
                    />
                </div>
                <Button
                    className={styles.Button}
                    variant="contained"
                    size="large"
                >Enter</Button>
            </div>
        </>
    );
}