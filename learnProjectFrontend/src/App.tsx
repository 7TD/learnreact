import React from 'react';
import {TestPage} from './components/TestPage/TestPage';
import './App.scss';
import {Route, Routes} from "react-router-dom";
import {Layout} from "./components/Layout/Layout";
import {Menu} from "./pages/Menu/Menu";
import {About} from "./pages/About/About";
import {Gallery} from "./pages/Gallery/Gallery";
import {Login} from "./pages/Login/Login";
import {Questionnaire} from "./pages/Questionnaire/Questionnaire";
import {Register} from "./pages/Register/Register";
import {MyTable} from "./pages/Table/MyTable";
import { MyDataGrid } from './pages/DataGrid/MyDataGrid';
import { Header } from './components/Header';

function App() {
    return (
        <div>
            <Routes>
                <Route path="/" element={<Header />}>
                    <Route index element={<Menu />}/>
                    <Route path="about" element={<About />}/>
                    <Route path="gallery" element={<Gallery />}/>
                    <Route path="login" element={<Login />}/>
                    <Route path="questionnaire" element={<Questionnaire />}/>
                    <Route path="register" element={<Register />}/>
                    <Route path="table" element={<MyTable />} />
                    <Route path="datagrid" element={<MyDataGrid />} />
                </Route>
                <Route path="/testpage" element={<TestPage />} />
            </Routes>
        </div>
    );
}

export default App;
