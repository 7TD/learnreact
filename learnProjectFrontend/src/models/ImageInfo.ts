export interface ImageInfo {
    id: number;
    photo: Blob;
}