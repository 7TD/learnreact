
export interface Car {
    id: number;
    brand: string;
    model: string;
    repair: boolean;
    custom: boolean;
    creatingDate: Date;
    endDate: Date;
    servicePrice: number;
}

export interface HeadCell {
    disablePadding: boolean;
    id: keyof Car;
    label: string;
    numeric: boolean;
}

export type Order = 'asc' | 'desc';
