import { AppBar, Avatar, Box, Button, IconButton, Menu, MenuItem, Tab, Tabs, Toolbar, Tooltip, Typography } from "@mui/material";
import React from "react";
import {Link, NavLink, Outlet, matchPath, useLocation, MemoryRouter} from 'react-router-dom';

const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];

const useRouteMatch = (patterns: readonly string[]) => {
    const { pathname } = useLocation();

    for (let i = 0; i < patterns.length; i++) {
        const pattern = patterns[i];
        const possibleMatch = matchPath(pattern, pathname);
        if (possibleMatch !== null) {
            return possibleMatch;
        }
    }

    return null;
}

export const Header = () => {
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    }

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    }

    const routeMatch = useRouteMatch(['/gallery', '/login', '/register', '/about', '/questionnaire', '/table', '/datagrid']);
    const currentTab = routeMatch?.pattern?.path

    return (
        <>
            <AppBar
                position="static"
            >
                <Toolbar>
                    <Typography
                        variant="h5"
                        component="span"
                        sx={{ flexGrow: 1 }}
                    >
                        Menu
                    </Typography>

                    <Box  sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                        <Tabs 
                            indicatorColor="secondary" 
                            textColor="inherit" 
                            // variant="fullWidth" 
                            sx={{ color: 'white' }} 
                            value={currentTab}
                        >
                            <Tab sx={{ color: 'white' }} label="Gallery" value="/gallery" to="/gallery" component={Link} />
                            <Tab label="Login" value="/login" to="/login" component={Link} />
                            <Tab label="Register" value="/register" to="/register" component={Link} />
                            <Tab label="About" value="/about" to="/about" component={Link} />
                            <Tab label="Questionnaire" value="/questionnaire" to="/questionnaire" component={Link} />
                            <Tab label="Table" value="/table" to="/table" component={Link} />
                            <Tab label="Data Grid" value="/datagrid" to="/datagrid" component={Link} />
                        </Tabs>
                    </Box>
                    
                    <Box sx={{ flexGrow: 0 }}>
                        <Tooltip title="Open settings">
                            <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                <Avatar alt="Remy Sharp">An</Avatar>
                            </IconButton>
                        </Tooltip>
                        <Menu
                            sx={{ mt: '45px' }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {settings.map((setting) => (
                                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                    <Typography textAlign="center">{setting}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                </Toolbar>
            </AppBar>
            <main>
                <Outlet />
            </main>
        </>
    );
}






