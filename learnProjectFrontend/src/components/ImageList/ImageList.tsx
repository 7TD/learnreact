import styles from './ImageList.module.scss';
import {ImageFiled} from "./ImageField/ImageFiled";
import React from "react";
import {ImageApi} from "../../api/imageRequest";
import {ImageInfo} from "../../models/ImageInfo";

interface ImageListProps {
    images: ImageInfo[]
    setActiveImageId: Function
    activeImageId: number
}

export const ImageList = ({ images, setActiveImageId, activeImageId } : ImageListProps) => {

    const [activeImage, setActiveImage] = React.useState<ImageInfo>({} as ImageInfo);

    React.useEffect(() => {
        setActiveImageId(activeImage.id);
    }, [activeImage])

    return (
        <>
            <div className={styles.list}>
                { images.map((image) => (
                    <div className={ activeImage === image ? styles.bg : '' }>
                        <ImageFiled
                            setActiveImageId={setActiveImageId}
                            activeImageId={activeImageId}
                            image={image}
                            key={image.id}
                        />
                    </div>
                )) }

            </div>
        </>
    );
}