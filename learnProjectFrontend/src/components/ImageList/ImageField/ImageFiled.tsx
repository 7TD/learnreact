import styles from './ImageFiled.module.scss';
import React from "react";
import {ImageInfo} from "../../../models/ImageInfo";

interface ImageProps {
    setActiveImageId: Function
    activeImageId: number
    image: ImageInfo
}

export const ImageFiled = ({image, setActiveImageId, activeImageId} : ImageProps) => {

    const onActive = () => {
        setActiveImageId(image.id);
    }

    return (
        <>
            <div className={styles.field}>
                <p
                    onClick={onActive}
                    className={activeImageId === image.id ? styles.fieldActive : styles.fieldDisable}
                >image id: { image.id }</p>
            </div>
        </>
    );
}