import React from 'react';
import styles from './Field.module.scss';
import {TextField} from "@mui/material";

interface FieldProps {
    lable: string
}

function Field({lable} : FieldProps) {
    return (
        <div className={styles.field}>
            <TextField
                 label={lable}
                 variant="filled"
                 className={styles.TextField}
            />
        </div>
    );
}

export default Field;